package com.testing.lab1.Controllers

import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.fxml.FXML
import javafx.scene.control.ComboBox
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import org.apache.commons.math3.analysis.UnivariateFunction
import org.apache.commons.math3.analysis.integration.TrapezoidIntegrator
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Serializable

class MainController {

    @FXML
    private lateinit var chooseTestComboBox: ComboBox<String>
    @FXML
    private lateinit var parametersTestArea: TextArea
    @FXML
    private lateinit var resultTestArea: TextArea
    @FXML
    private lateinit var countField: TextField

    private lateinit var command: Map<String, Serializable>
    private var arrayCommand: ArrayList<Map<String, Serializable>> = ArrayList()
    private var tests: ObservableList<String> = FXCollections.observableArrayList()

    fun initialize() {
        for (i in 0..2) {
            tests.add("Тест $i")
        }
        chooseTestComboBox.items = tests

        chooseTestComboBox.setOnAction {
            arrayCommand.clear()
            parametersTestArea.clear()
            command = CommandParam(parametersTestArea).generateCommand(chooseTestComboBox.selectionModel.selectedIndex)
            parametersTestArea.text = command["description"].toString().plus("\n")
        }
    }

    fun onClickStartTest() {
        resultTestArea.clear()
        println(command)
        for (i in arrayCommand) {
            executeCommand(i)
        }
    }

    fun onClickGenerate() {
        arrayCommand.clear()
        parametersTestArea.clear()
        parametersTestArea.text = command["description"].toString()
        for (i in 0 until countField.text.toInt()) {
            arrayCommand.add(CommandParam(parametersTestArea).generateCommand(chooseTestComboBox.selectionModel.selectedIndex))
            parametersTestArea.text += "\n\nНачало интервала = ${arrayCommand[i]["start"].toString()}, конец интервала = ${arrayCommand[i]["end"].toString()}, шаг = ${arrayCommand[i]["step"].toString()},\n" +
                    "номер метода = ${arrayCommand[i]["method"].toString()}, коэффициенты = ${arrayCommand[i]["coef"].toString()}"
        }

    }

    private fun executeCommand(command: Map<String, Serializable>) {
        try {
            var commandString: String = command["start"].toString().plus(" ") + command["end"].toString().plus(" ") +
                    command["step"].toString().plus(" ") + command["method"].toString().plus(" ")
            val array = command["coef"] as ArrayList<*>
            for (i in array.iterator()) {
                commandString += i.toString().plus(" ")
            }
            println(commandString)

            val output = Runtime.getRuntime().exec("Integral3x.exe $commandString\"")
            output.waitFor()
            val reader = BufferedReader(InputStreamReader(output.inputStream, "cp1251"))
            val line = reader.readLines()
            for (i in line) {
                resultTestArea.text += i + "\n"
            }
            output.destroy()
            val answer = calculator(command)
            resultTestArea.text += "answer = $answer \n\n"
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun calculator(command: Map<String, Serializable>): Double {
        val integral = TrapezoidIntegrator()
        val array = command["coef"] as ArrayList<Int>
        val f = UnivariateFunction { x ->
            var FS = 0.0
            for (i in 0 until array.size) {
                FS += array[i] * Math.pow(x, i.toDouble())
            }
            FS
        }
        val start: Double = command["start"].toString().toDouble()
        val end: Double = command["end"].toString().toDouble()
        return integral.integrate(10000, f, start, end)
    }
}