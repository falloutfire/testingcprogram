package com.testing.lab1.Controllers

import javafx.scene.control.TextArea
import java.io.Serializable
import kotlin.random.Random

class CommandParam(
    var textArea: TextArea,
    var description: String = "",
    var start: String = "",
    var end: String = "",
    var step: String = "",
    var method: String = "",
    var countParam: String = "",
    var param: String = ""
) {

    fun generateCommand(numberTest: Int): Map<String, Serializable> {
        val coefArray = arrayListOf<Int>()
        when (numberTest) {
            0 -> {
                description = "Ввод данных согласно ТЗ"
                start = Random.nextInt(-10, 5).toString().plus(" ")
                end = Random.nextInt(5, 10).toString().plus(" ")
                step = String.format("%.6f", Random.nextDouble(0.000001, 0.5)).plus(" ")
                method = Random.nextInt(1, 3).toString().plus(" ")
                val count = Random.nextInt(1, 5)
                countParam = count.toString().plus(" ")
                for (i in 0 until count) {
                    val coef = Random.nextInt(1, 5)
                    coefArray.add(coef)
                    param += coef.toString().plus(" ")
                }
            }
            1 -> {
                description = "Ввод данных, знак доли числа заменен на точку"
                start = Random.nextInt(-10, 5).toString().plus(" ")
                end = Random.nextInt(5, 10).toString().plus(" ")
                step = String.format("%.6f", Random.nextDouble(0.000001, 0.5)).plus(" ").replace(",", ".")
                method = Random.nextInt(1, 3).toString().plus(" ")
                val count = Random.nextInt(1, 5)
                countParam = count.toString().plus(" ")
                for (i in 0 until count) {
                    val coef = Random.nextInt(1, 5)
                    coefArray.add(coef)
                    param += coef.toString().plus(" ")
                }
            }
            2 -> {
                description = "Ввод данных, знак доли числа заменен на точку, конец интервала больше начала"
                start = Random.nextInt(5, 10).toString().plus(" ")
                end = Random.nextInt(0, 4).toString().plus(" ")
                step = String.format("%.6f", Random.nextDouble(0.000001, 0.5)).plus(" ").replace(",", ".")
                method = Random.nextInt(1, 3).toString().plus(" ")
                val count = Random.nextInt(1, 5)
                countParam = count.toString().plus(" ")
                for (i in 0 until count) {
                    val coef = Random.nextInt(1, 5)
                    coefArray.add(coef)
                    param += coef.toString().plus(" ")
                }
            }
            /*3 -> {
                description = "Ввод данных, количесвто коэф. - отричцательное число"
                start = Random.nextInt(-10, 5).toString().plus(" ")
                end = Random.nextInt(5, 10).toString().plus(" ")
                step = String.format("%.6f", Random.nextDouble(0.000001, 0.5)).plus(" ")
                method = Random.nextInt(1, 3).toString().plus(" ")
                val count = Random.nextInt(-5, -1)
                countParam = count.toString().plus(" ")
                for (i in count until 0) {
                    val coef = Random.nextInt(1, 5)
                    coefArray.add(coef)
                    param += coef.toString().plus(" ")
                }
            }*/
        }
        //textArea.text =
                "Начало интервала = $start, конец интервала = $end,\nшаг = $step, номер метода = $method," +
                "\nкол-во коэф. полинома = $countParam, коэффициенты = $param"


        return mapOf(
            "description" to description,
            "start" to start,
            "end" to end,
            "step" to step,
            "method" to method,
            "coef" to coefArray
        )
        //return start + end + step + method /*+ countParam*/ + param
    }
}